function accordion() {
	$(".accordion__trigger").click(function () {
		let controlled = $(this).attr('aria-controls');
	    $(".accordion__trigger").attr('aria-expanded', false);
	    $(".accordion__panel").attr('hidden', '');
	    $(this).attr('aria-expanded', true);
	    $('#'+controlled).removeAttr('hidden');
	});
}

function initSite() {
	accordion();
}

$(document).ready(function() {
	initSite(); 
});