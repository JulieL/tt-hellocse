// Requis
var gulp = require('gulp');

// Include plugins
var plugins = require('gulp-load-plugins')(); // tous les plugins de package.json
var notify = require("gulp-notify");
var plumber = require('gulp-plumber');

// Tâche "build" = LESS + autoprefixer + CSScomb + beautify (source -> destination)
gulp.task('css', function () {
    return gulp.src('css/styles.less')
    .pipe(plugins.less())
    .pipe(plugins.csscomb())
    .pipe(plugins.cssbeautify({indent: '   '}))
    .pipe(plugins.autoprefixer())
    .pipe(gulp.dest('css/'))
    .pipe(notify({ message: 'css actualisé' }))
    .pipe(gulp.dest('css'));
});

// Tache "less"
gulp.task('less', function () {
    return gulp.src('less/style.less')
    .pipe(plugins.less())
    .pipe(plugins.csscomb())
    .pipe(plugins.cssbeautify({indent: '    '}))
    .pipe(plugins.autoprefixer())
    .pipe(notify({ message: 'less actualisé' }))
    .pipe(gulp.dest('css'));
});

// Tâche "build"
gulp.task('build', ['less']);

// Tâche "watch" = je surveille *less
gulp.task('watch', function () {                    //gulp watch pour mettre en route
    gulp.watch('less/*.less', ['less']);
    gulp.watch('less/**/*.less', ['less']);
});

// Tâche par défaut
gulp.task('default', ['build']);
